import { Component } from "react";
import Item from "./components/item";
import Header from "./components/header";
import "./App.scss";

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			items: [],
			numOfFav: localStorage.getItem("numOfFavorites"),
			itemsInCart: 0,
			numOfFavorites: 0,
			favorite: false,
		};
	}

	componentDidMount() {
		fetch("./index.json")
			.then((res) => res.json())
			.then((data) => {
				this.setState({ items: data });
			});
	}

	addToCart = () => {
		this.setState((prev) => ({ itemsInCart: prev.itemsInCart + 1 }));
		localStorage.setItem("ItemsInCart", `${this.state.itemsInCart + 1}`);
	};

	favoriteChange = () => {
		const currentFavorite = this.state.favorite;
		this.setState({ favorite: !currentFavorite });
		if (this.state.favorite) {
			this.setState((prev) => ({
				numOfFavorites: prev.numOfFavorites + 1,
			}));
		} else {
			localStorage.setItem(
				"numOfFavorites",
				`${this.state.numOfFavorites + 1}`
			);
		}
	};

	render() {
		const newItems = this.state.items.map((item) => {
			return (
				<Item
					image={item.url}
					price={item.price}
					name={item.name}
					key={item.article}
					color={item.color}
					addToCart={this.addToCart}
					favoriteChange={this.favoriteChange}
					favorite={this.state.favorite}></Item>
			);
		});
		return (
			<div className="App">
				<Header numOfFav={localStorage.getItem("numOfFavorites")}></Header>
				<div className="items">{newItems}</div>
			</div>
		);
	}
}

export default App;
