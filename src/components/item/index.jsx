import { Component } from "react";
import Modal from "../modal";
import PropTypes from "prop-types";
import "./style.scss";

export default class Item extends Component {
	constructor(props) {
		super(props);

		this.state = {
			header: "",
			closeButton: "",
			text: "",
			actions: "",
			modalClosed: true,
			favorite: false,
		};
	}

	showModal() {
		this.setState({
			modalClosed: false,
			header: "Are you sure you want to add this item to cart?",
			closeButton: false,
			text: "Are you sure?",
			actions: (
				<div className="modal__actions">
					<button
						className="modal__actions-button"
						onClick={() => {
							this.props.addToCart();
							this.handleClose();
						}}>
						Yes
					</button>
					<button
						className="modal__actions-button"
						onClick={() => this.handleClose()}>
						No
					</button>
				</div>
			),
		});
	}

	handleClose = () => {
		this.setState({ modalClosed: true });
	};

	render() {
		return (
			<>
				<div className="item">
					<img src={this.props.image} alt="lamp" className="item__image" />
					<div className="item__content">
						<h4 className="item__name">{this.props.name}</h4>
						<svg
							onClick={() => {
								this.props.favoriteChange();
								const currentFav = this.state.favorite;
								this.setState({ favorite: !currentFav });
							}}
							className={
								this.state.favorite ? "item__star-active" : "item__star"
							}
							height="20px"
							width="20px"
							version="1.1"
							id="Capa_1"
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 47.94 47.94">
							<g id="SVGRepo_bgCarrier" strokeWidth="0"></g>
							<g
								id="SVGRepo_tracerCarrier"
								strokeLinecap="round"
								strokeLinejoin="round"></g>
							<g id="SVGRepo_iconCarrier">
								{" "}
								<path d="M26.285,2.486l5.407,10.956c0.376,0.762,1.103,1.29,1.944,1.412l12.091,1.757 c2.118,0.308,2.963,2.91,1.431,4.403l-8.749,8.528c-0.608,0.593-0.886,1.448-0.742,2.285l2.065,12.042 c0.362,2.109-1.852,3.717-3.746,2.722l-10.814-5.685c-0.752-0.395-1.651-0.395-2.403,0l-10.814,5.685 c-1.894,0.996-4.108-0.613-3.746-2.722l2.065-12.042c0.144-0.837-0.134-1.692-0.742-2.285l-8.749-8.528 c-1.532-1.494-0.687-4.096,1.431-4.403l12.091-1.757c0.841-0.122,1.568-0.65,1.944-1.412l5.407-10.956 C22.602,0.567,25.338,0.567,26.285,2.486z"></path>{" "}
							</g>
						</svg>
						<p className="item__description">
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere,
							beatae enim!
						</p>
						<div className="item__bottom">
							<span className="item__price">${this.props.price}</span>
							<button className="item__button" onClick={() => this.showModal()}>
								Add to cart
							</button>
						</div>
					</div>
				</div>
				{this.state.modalClosed ? null : (
					<Modal
						header={this.state.header}
						closeButton={this.state.closeButton}
						text={this.state.text}
						actions={this.state.actions}
						modalClosed={this.state.modalClosed}
						close={this.handleClose}></Modal>
				)}
			</>
		);
	}
}

Item.propTypes = {
	addToCart: PropTypes.func.isRequired,
	image: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	favoriteChange: PropTypes.func.isRequired,
	price: PropTypes.number.isRequired,
};
