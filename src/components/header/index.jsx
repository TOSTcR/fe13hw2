import { Component } from "react";
import PropTypes from "prop-types";
import "./styles.scss";

export default class Header extends Component {
	render() {
		const items = localStorage.getItem("ItemsInCart");
		return (
			<nav className="nav">
				<span className="nav__cart">
					Cart(Currently in cart {items ? items : 0} items)
				</span>
				<span className="nav__favorites">
					Number of favorite items:{" "}
					{this.props.numOfFav ? this.props.numOfFav : 0}
				</span>
			</nav>
		);
	}
}

Header.propTypes = {
	numOfFav: PropTypes.string.isRequired,
};
